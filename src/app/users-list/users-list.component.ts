import {Component, OnInit} from '@angular/core';
import {HTTPService} from '../http.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})


export class UsersListComponent implements OnInit {
  users = [];
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'email'];

  constructor(private httpService: HTTPService) {
  }

  ngOnInit(): void {
    this.httpService.getAllUsers().subscribe(users => {
      this.users = users;
    }, error => console.log(error));
  }

}
